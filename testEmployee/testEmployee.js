const { employeeId, swapData, dataOfCompanyPowerpuff, dataWithoutParticularId, sortData, evenAndBirthdayDate, groupDataOnCompanieName } = require("../employee")
const data = require('../data.json')

const dataObj = data.employees
const id = [2, 13, 23]

async function employeeAsyncAwaitCallbacks() {
    try {
        const response = await employeeId(id, dataObj)
        console.log(response)
    }
    catch (err) {
        console.log(err.message)
    }
    try {
        const response = await groupDataOnCompanieName(dataObj)
    }
    catch (err) {
        console.log(err.message)
    }
    try {
        const response = await dataOfCompanyPowerpuff(dataObj)
    }
    catch (err) {
        console.log(err.message)
    }
    try {
        const response = await dataWithoutParticularId(dataObj)
    }
    catch (err) {
        console.log(err.message)
    }
    try {
        const response = await sortData(dataObj)
    }
    catch (err) {
        console.log(err.message)
    }
    try {
        const response = await swapData(dataObj)
    }
    catch (err) {
        console.log(err.message)
    }
    try {
        const response = await evenAndBirthdayDate(dataObj)
    }
    catch (err) {
        console.log(err.message)
    }
}

employeeAsyncAwaitCallbacks();